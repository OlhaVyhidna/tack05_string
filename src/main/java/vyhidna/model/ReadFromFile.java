package vyhidna.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadFromFile {

  private static Logger LOG = LogManager.getLogger();
  String fileName = "default.txt";

  public ReadFromFile() {
  }

  public ReadFromFile(String fileName) {
    this.fileName = fileName;
  }

  public List<String> readFromFile() throws FileNotFoundException {
    File file = new File(fileName);
    List<String> text = new ArrayList<>();
    if (!file.exists()) {
      LOG.error("this file does not exist");
      throw new FileNotFoundException();
    }
    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
      String line = bufferedReader.readLine();
      while (line != null) {
        text.add(line);
        line = bufferedReader.readLine();
      }
    } catch (Exception e) {

    }
    return text;
  }
}
