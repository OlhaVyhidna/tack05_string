package vyhidna.model;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalisationClass {

  Locale defaultLanguage = new Locale("en", "English");
  Locale greek = new Locale("el", "Greek");
  Locale spanish = new Locale("es", "Spanish");
  Locale ukranian = new Locale("uk", "Ukrainian");

  public void print(Locale locale){
    ResourceBundle message = ResourceBundle.getBundle("menu_resource", locale);
    System.out.println(message.getString("choose_the_language"));
  }

}
