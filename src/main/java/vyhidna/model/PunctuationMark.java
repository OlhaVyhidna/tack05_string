package vyhidna.model;

public class PunctuationMark {
  private String mark;

  public PunctuationMark(String mark) {
    this.mark = mark;
  }

  public String getMark() {
    return mark;
  }

  public void setMark(String mark) {
    this.mark = mark;
  }

  @Override
  public String toString() {
    return "PunctuationMark{" +
        "mark='" + mark + '\'' +
        '}';
  }
}
