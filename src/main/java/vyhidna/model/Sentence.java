package vyhidna.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
  private final static String WORD_REGEX = "\\s*\\w+[^\\s\\?\\,\\.\\!\\:\\;]*";
  private final static String PUNCTUATION_MARK_REGEX = "\\s*[\\?\\,\\.\\!\\:\\;]";
  String sentence;
  List<Word> words;
  List<PunctuationMark> punctuationMarks;


  public Sentence(String sentence) {
    this.sentence = sentence;
    this.words = new ArrayList<>();
    this.punctuationMarks = new ArrayList<>();
    parseSentence(sentence);
  }

  private void parseSentence(String sentence){
    Pattern wordPattern = Pattern.compile(Sentence.WORD_REGEX);
    Pattern punctuationPattern = Pattern.compile(Sentence.PUNCTUATION_MARK_REGEX);
    Matcher wordMatcher = wordPattern.matcher(sentence);
    Matcher punctuationMatcher = punctuationPattern.matcher(sentence);
    while (wordMatcher.find()){
        this.words.add(new Word(sentence.substring(wordMatcher.start(), wordMatcher.end()).trim()));
    }

    while (punctuationMatcher.find()){
      this.punctuationMarks.add(new PunctuationMark(sentence.substring(punctuationMatcher.start(),
            punctuationMatcher.end()).trim()));
    }
  }

  public String getSentence() {
    return sentence;
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public List<Word> getWords() {
    return words;
  }

  public void setWords(List<Word> words) {
    this.words = words;
  }

  public List<PunctuationMark> getPunctuationMarks() {
    return punctuationMarks;
  }

  public void setPunctuationMarks(List<PunctuationMark> punctuationMarks) {
    this.punctuationMarks = punctuationMarks;
  }

  @Override
  public String toString() {
    return "Sentence{" +
        "sentence='" + sentence + '\'' +
        ", words=" + words +
        ", punctuationMarks=" + punctuationMarks +
        '}';
  }
}
