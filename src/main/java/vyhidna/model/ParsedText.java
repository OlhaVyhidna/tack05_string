package vyhidna.model;

import java.util.ArrayList;
import java.util.List;

public class ParsedText {

  private List<Sentence> sentences;

  public ParsedText(List<String> inputtedText) {
    this();
    convertInputtedText(inputtedText);

  }

  public ParsedText() {
    this.sentences = new ArrayList<>();
  }

  private void convertInputtedText(List<String> inputtedText) {
    for (String s : inputtedText) {
      s = s.replaceAll("(\\t|\\s+)", " ");
      if (s.length() > 0) {
        sentences.add(new Sentence(s));
      }
    }
  }

  public List<Sentence> getSentences() {
    return sentences;
  }

  public void setSentences(List<Sentence> sentences) {
    this.sentences = sentences;
  }
}
