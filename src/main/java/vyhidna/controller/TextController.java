package vyhidna.controller;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vyhidna.model.ParsedText;
import vyhidna.model.Sentence;
import vyhidna.model.Word;

public class TextController {

  private ParsedText parsedText;
  private static Logger LOG = LogManager.getLogger();

  public TextController(ParsedText parsedText) {
    this.parsedText = parsedText;
  }

  public void firstTask(){
getWordsList().stream()
    .max(Comparator.comparing(this::numberOfSentenceWhereWwordRepeatS))
    .ifPresent(word->LOG.info("Number of sentences " + numberOfSentenceWhereWwordRepeatS(word)));
  }

  public void secondTask() {
    parsedText.getSentences().stream()
        .sorted(Comparator.comparingInt(s -> s.getWords().size()))
        .forEach(
            sentence -> LOG.info(sentence.getSentence() + " size: " + sentence.getWords().size()));

  }

  private int numberOfSentenceWhereWwordRepeatS(String word){
    String wordPattern = "\\b(?i)" + word + "\\b";
    int[] numberOfRepeats = {0};
    Pattern pattern = Pattern.compile(wordPattern);
    parsedText.getSentences().forEach(sentence -> {
      if (pattern.matcher(sentence.getSentence()).find()){
        numberOfRepeats[0]++;
      }
    });
//    parsedText.getSentences().stream().flatMap(sentence -> sentence.getWords().stream())
//        .forEach(word1 -> {
//          if (pattern.matcher(word1.getWord()).find()) {
//            numberOfRepeats[0]++;
//          }
//        });
    return numberOfRepeats[0];
  }

  public void thirdTask() {
    List<Word> firstSentenceWords = parsedText.getSentences().get(0).getWords();
    String uniWord = "";
    int numberOfRepeats = 0;
    for (Word word : firstSentenceWords) {
      for (int i = 1; i < parsedText.getSentences().size(); i++) {
        Sentence sentence = parsedText.getSentences().get(i);
        for (Word sentenceWord : sentence.getWords()) {
          if (sentenceWord.getWord().equals(word.getWord())) {
            numberOfRepeats++;
          }
        }
      }
      if (numberOfRepeats == 0) {
        uniWord = word.getWord();
        break;
      }
    }
    if (!uniWord.equals("")) {
      LOG.info(uniWord);
    } else {
      LOG.info("The first sentence does not contains unique words");
    }
  }

  public void fourthTask(int wordLength) {
    String regexForWordLength = ".{" + wordLength + "}";
    String regexForLineEnd = ".+[$\\?]";
    parsedText.getSentences().stream()
        .filter(sentence -> sentence.getSentence().matches(regexForLineEnd))
        .flatMap(sentence -> sentence.getWords().stream())
        .filter(word -> word.getWord().matches(regexForWordLength)).distinct()
        .forEach(word -> LOG.info(word.getWord()));
  }

  public void fifthTask() {
    String vowelLetterPattern = "\\b[aeiou]\\w*";
    Pattern pattern = Pattern.compile(vowelLetterPattern, Pattern.CASE_INSENSITIVE);

    parsedText.getSentences().stream()
        .map(sentence -> pattern.matcher(sentence.getSentence())
            .replaceFirst(getWordForReplacement(sentence))).forEach(s -> LOG.info(s));
  }

  private String getWordForReplacement(Sentence sentence) {
    return sentence.getWords().stream()
        .max(Comparator.comparing(word -> word.getWord().length())).get().getWord();
  }

  public void sixthTask() {
    parsedText.getSentences().stream()
        .flatMap(sentence -> sentence.getWords().stream())
        .map(Word::getWord)
        .distinct()
        .collect(Collectors.groupingBy(s -> s.charAt(0))).forEach((k, v) -> {
      System.out.print("\t");
      v.stream().distinct().forEach(System.out::println);
    });
  }

  public void seventhTask() {
    parsedText.getSentences().stream()
        .flatMap(sentence -> sentence.getWords().stream())
        .map(word -> word.getWord()).distinct()
        .sorted(Comparator.comparingInt(this::percentageOfLetters))
        .forEach(s -> LOG.info(s + " percentage " + percentageOfLetters(s)));
  }

  private int percentageOfLetters(String s) {
    String vowelsPattern = "(?i)[aeiou]";
    int length = s.length();
    int numberOfVowels = 0;

    Pattern pattern = Pattern.compile(vowelsPattern);
    Matcher matcher = pattern.matcher(s);
    while (matcher.find()) {
      numberOfVowels++;
    }
    double i = (numberOfVowels / (double) length) * 100;
    return (int) i;
  }

  public void eighthTask(){
    String vowelWord = "\\b(?i)[aeiou]";
    Pattern pattern = Pattern.compile(vowelWord);
    getWordsList().stream()
        .filter(w->pattern.matcher(w.charAt(0)+"").find())
        .sorted(((s1,s2)->findConsonantal(s1).compareToIgnoreCase(findConsonantal(s2))))
        .forEach(word->LOG.info(word));
  }

  public String findConsonantal(String word){
    String[] split = word.split("");
    String vowelWord = "\\b(?i)[aeiou]";
    Pattern pattern = Pattern.compile(vowelWord);
    for (String s : split) {
      if (!pattern.matcher(s).find()){
        System.out.println(s);
        return s;
      }
    }
    return "";
  }

  public void ninthTask(String letterToCount) {
    parsedText.getSentences().stream()
        .flatMap(sentence -> sentence.getWords().stream())
        .map(Word::getWord).distinct()
        .sorted(Comparator.comparingInt(s -> countOfSymbolRepeats(letterToCount, s.toString()))
            .thenComparing((w1, w2) -> w1.toString().compareToIgnoreCase(w2.toString())))
        .forEach(s -> LOG.info(s + " letter " + letterToCount + " number of repeats "
            + countOfSymbolRepeats(letterToCount, s)));
  }

  private int countOfSymbolRepeats(String symbol, String word) {
    String regexPattern = "(?i)[" + symbol + "]";
    int countOfRepeats = 0;
    Pattern pattern = Pattern.compile(regexPattern);
    Matcher matcher = pattern.matcher(word);
    while (matcher.find()) {
      countOfRepeats++;
    }
    return countOfRepeats;
  }

  public void tenthTask() {
    parsedText.getSentences().stream()
        .flatMap(sentence -> sentence.getWords().stream())
        .map(Word::getWord)
        .distinct()
        .sorted((s1, s2) -> countWordRepeat(s2) - countWordRepeat(s1))
        .forEach(s -> LOG.info(s + " number of repeats " + countWordRepeat(s)));
  }

  private int countWordRepeat(String word) {
    String wordPattern = "\\b" + word + "\\b";
    int[] numberOfRepeats = {0};
    Pattern pattern = Pattern.compile(wordPattern);
    parsedText.getSentences().stream().flatMap(sentence -> sentence.getWords().stream())
        .forEach(word1 -> {
          if (pattern.matcher(word1.getWord()).find()) {
            numberOfRepeats[0]++;
          }
        });
    return numberOfRepeats[0];
  }

  public void eleventhTask(String startSymbol, String endSymbol) {
    String substringPattern = startSymbol + ".+" + endSymbol;
    Pattern pattern = Pattern.compile(substringPattern, Pattern.CASE_INSENSITIVE);
    parsedText.getSentences().forEach(sentence -> {
      Matcher matcher = pattern.matcher(sentence.getSentence());
      if (matcher.find()) {
        LOG.info(sentence.getSentence().substring(matcher.start(), matcher.end()));
      }
    });
  }

  public void twelveTask(int wordLength) {
    String wordPattern = "\\b[^aeiou\\s]\\w{" + (wordLength - 1) + "}\\b";
    parsedText.getSentences().forEach(sentence -> {
      sentence.setSentence(sentence.getSentence().replaceAll(wordPattern, ""));
      LOG.info(sentence.getSentence());
    });
  }

  public void thirteenthTask(String symbol) {
    parsedText.getSentences().stream().flatMap(sentence -> sentence.getWords().stream())
        .distinct()
        .sorted(Comparator.comparingInt(w -> countOfSymbolRepeats(symbol, w.toString()))
            .reversed()
            .thenComparing((w1, w2) -> w1.toString().compareToIgnoreCase(w2.toString())))
        .forEach(word -> LOG.info(word.getWord()));

  }

  public void fourteenthTask() {
    List<String> wordsList = getWordsList();
    String palindrome = "";
    for (String s : wordsList) {
      String reverse = new StringBuilder(s).reverse().toString();
      if (s.equalsIgnoreCase(reverse)) {
        if (s.length() > palindrome.length()) {
          palindrome = s;
        }
      }
    }
    if (palindrome.length() > 0) {
      LOG.info("palindrome is: " + palindrome);
    } else {
      LOG.info("there is no palindrome in the text");
    }
  }

  public void fifteenthTask() {
    getWordsList().forEach(w->LOG.info(deleteRepeats(w)));
  }

  public String deleteRepeats(String word){
    String startLetter = word.charAt(0) + "";
    String endLetter = word.charAt(word.length()-1) + "";
   word =  word.replaceAll(startLetter, "");
    word = word.replaceAll(endLetter, "");
    return startLetter+word+endLetter;
  }

  private List<String> getWordsList() {
    return parsedText.getSentences().stream()
        .flatMap(sentence -> sentence.getWords().stream())
        .distinct()
        .map(Word::getWord)
        .collect(Collectors.toList());
  }

  public void sixteenthTask(int wordLength, String substring){
    String wordLengthPattern = "\\b\\w{" + wordLength +"}\\b";
parsedText.getSentences().stream()
    .map(sentence -> sentence.getSentence()
        .replaceAll(wordLengthPattern, substring)).
    forEach(s->LOG.info(s));
  }
}

